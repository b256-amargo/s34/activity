// Creating s Simple Server using ExpressJS Framework

	const express = require("express");
	// use require() directive to load the express module/package
	// allows us to access the methods and functions that will help us create a server

	const app = express();
	// creates an express application and stores this in a constant called "app" (thus "app" is now our server)

	const port = 3000;
	// server's port

	app.use(express.json());
	// will convert all incoming raw data into json, unlike on previous sessions where they're converted one at a time
	// allows the server to handle data from requests (thus allows the app to read json data)
	// .use is called a middleware
	// Middleware is software that provides common services and capabilities to applications outside of what’s offered by the operating system
	// API management is one of the common application of middlewares.
	app.use(express.urlencoded({extended: true}));
	// .urlencoded allows your application to read data from forms
	// By default, information received from the url can only be received as a string or an array
	// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application

// Creating Routes

	app.get("/", (request, response) => {
		response.send("Hello Warudo")
	});
	// GET method

	app.post("/", (req, response) => {
		response.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`)
		// will send after entering the input on postman
	});
	// POST method

	let users = [];
	// mock database

	app.post("/signup", (req, res) => {
		if(req.body.username !== "" && req.body.password !== ""){
			users.push(req.body);
			res.send(`User ${req.body.username} successfully registered!`)
			// will send after entering the input on postman
		}
		else{
			res.send(`Please input both username and password!`)
			// will send after entering the input on postman
		};
		console.log(req.body)
		// will log on gitbash
	});
	// sign up method

	app.put("/changepassword", (req, res) => {
		let message;
		// will store the message to be sent back to the client

		for(let userIndex = 0; userIndex < users.length; userIndex ++){
			if(req.body.username == users[userIndex].username){
				users[userIndex].password = req.body.password;
				message = `User ${req.body.username} has successfully changed their password.`;
				break;
				// loop broken since the account to be modified is already found
			}
			else{
				message = `User ${req.body.username} does not exist.`;
			};
		};

		res.send(message);
		console.log(users);
	})
	// change password method

// --------------------------------------------------------------

// ACTIVITY

// (1)
app.get("/home", (request, response) => {
	response.send("Welcome to the home page");
});


// (2)
app.get("/users", (request, response) => {
	response.send(users);
});


// ---------------------------------------------------------------

app.listen(port, () => console.log(`Server is listening at localhost:${port}`));